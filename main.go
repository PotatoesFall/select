package main

import (
	"os"

	"github.com/charmbracelet/huh"
)

func main() {
	if len(os.Args) == 1 {
		panic("no choices supplied")
	}
	s := choose("Choose", os.Args[1:])
	err := os.WriteFile("/tmp/choice.txt", []byte(s), 0o777)
	if err != nil {
		panic(err)
	}
}

// func getAnalyticsInstanceName() {
// 	env := choose("Environment", []string{"prd", "dev"})
// 	kind := choose("Consumer type", []string{"usr", "org"})
// 	region := choose("Region", []string{"america", "europe", "asia"})

// 	fmt.Fprintf(os.Stderr, "%s-%s-%s", env, kind, region)
// }

func choose(prompt string, options []string) string {
	var opts []huh.Option[int]
	for i, o := range options {
		opts = append(opts, huh.NewOption(o, i))
	}

	var i int

	err := huh.NewSelect[int]().
		Title(prompt).
		Options(
			opts...,
		).Value(&i).Run()
	if err != nil {
		panic(err)
	}
	return options[i]
}
